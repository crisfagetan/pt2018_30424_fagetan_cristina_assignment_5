package control;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MonitoredData {
	Date startTime;
	Date endTime;
	String activity;

	public MonitoredData(Date startTime, Date endTime, String activity) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.activity = activity;
	}

	public MonitoredData() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String dateToString(Date d) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		return format.format(d);
	}

	@Override
	public String toString() {
		return "Start time: " + dateToString(getStartTime()) + " End time: " + dateToString(getEndTime())
				+ " Activity: " + getActivity();
	}

}

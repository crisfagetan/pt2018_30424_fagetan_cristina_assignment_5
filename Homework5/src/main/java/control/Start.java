package control;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Start {

	@SuppressWarnings("deprecation")
	public static void main(String args[]) throws FileNotFoundException, UnsupportedEncodingException {

		List<MonitoredData> monitoredData = new ArrayList<MonitoredData>();
		String fileName = "D:\\PT2018\\pt2018_30424_fagetan_cristina_assignment_5\\Activities.txt";

		// ------------TASK1 - read with stream and create "data"------------------
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			monitoredData = stream.map(a -> a.split("\\s\\s+"))
					.map(arr -> new MonitoredData(toDate(arr[0]), toDate(arr[1]), arr[2])).collect(Collectors.toList());
		} catch (IOException e) {
			e.printStackTrace();
		}
		// System.out.println("Task1:");
		// monitoredData.forEach(System.out::println); //proof that task1 works

		// ------------TASK2 count days------------------
		// method#1
		List<String> distinctStartTime = monitoredData.stream().map(m -> m.getStartTime().toString().substring(0, 10))
				.distinct().collect(Collectors.toList());
		// for our input, distinctEndTime is useless
		List<String> distinctEndTime = monitoredData.stream().map(m -> m.getEndTime().toString().substring(0, 10))
				.distinct().collect(Collectors.toList());
		Set<String> distinctDays = new HashSet<String>(distinctStartTime);
		distinctDays.addAll(distinctEndTime);

		// method#2 -- I can do this because the days are consecutive
		long diffInMillies = Math.abs(monitoredData.get(monitoredData.size() - 1).getEndTime().getTime()
				- monitoredData.get(0).getStartTime().getTime());
		long diff = TimeUnit.DAYS.convert(diffInMillies, TimeUnit.MILLISECONDS);

		System.out.println("Task2 - the number of days in the log is:");
		System.out.println("Method 1: " + distinctDays.size());
		System.out.println("Method 2: " + (diff + 1)); // need to add 1 because it dosen't count the first date/last
														// date, just how many days are between them

		// ------------TASK3 count activity occurrencies-----------------
		PrintWriter task3 = new PrintWriter("task3.txt");
		Map<String, Long> map = monitoredData.stream()
				.collect(Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting()));
		map.forEach((key, value) -> task3.println(key + ": " + value));
		task3.close();

		// -------------TASK4 count activity occurrencies / day----------------------
		PrintWriter task4 = new PrintWriter("task4.txt");
		Map<Object, Map<String, Long>> map2 = monitoredData.stream()
				.collect(Collectors.groupingBy(x -> x.getStartTime().getDate(),
						Collectors.groupingBy(MonitoredData::getActivity, Collectors.counting())));
		map2.forEach((key, value) -> task4.println(key + ": " + value));
		task4.close();

		// -------------TASK5 ---------------------
		// --------for each line get duration
		PrintWriter task5print = new PrintWriter("task5print.txt");

		monitoredData.forEach((x) -> task5print
				.println(String.format("Start: %s  End: %s,  Activity: %s, Time consumed: %d hours, %d min, %d sec",
						x.getStartTime(), x.getEndTime(), x.getActivity(),
						(int) (((Math.abs(x.getEndTime().getTime() - x.getStartTime().getTime())) / (1000 * 60 * 60))),
						(int) (((Math.abs(x.getEndTime().getTime() - x.getStartTime().getTime())) / (1000 * 60)) % 60),
						(int) ((Math.abs(x.getEndTime().getTime() - x.getStartTime().getTime())) / 1000) % 60)));
		task5print.close();
		// ---------for each activity get total duration------------------
		PrintWriter task5 = new PrintWriter("task5.txt");
		Map<Object, Long> map3 = monitoredData.stream().collect(Collectors.groupingBy(x -> x.getActivity().toString(),
				Collectors.summingLong(s -> (Math.abs(s.getEndTime().getTime() - s.getStartTime().getTime())))));
		map3.forEach((key, value) -> task5.println(key + ": " + String.format("%d hours, %d min, %d sec",
				(int) ((value / (1000 * 60 * 60))), (int) ((value / (1000 * 60)) % 60), (int) (value / 1000) % 60)));

		// --------duration grater than 10 hours------
		task5.println();
		task5.println("Activities with duration grater than 10 hours:");

		map3.forEach((key, value) -> {
			if ((int) ((value / (1000 * 60 * 60))) > 10)
				task5.println(key + ": " + String.format("%d hours, %d min, %d sec", (int) ((value / (1000 * 60 * 60))),
						(int) ((value / (1000 * 60)) % 60), (int) (value / 1000) % 60));
		});
		task5.close();

		// -------TASK6----------------------------------------
		//activities with 90% of samples lasting <= 5 minutes
		PrintWriter task6 = new PrintWriter("task6.txt");
		Map<String, Map<Object, Long>> activity5Min = monitoredData.stream()
				.collect(Collectors.groupingBy(s -> s.getActivity().toString(), Collectors.groupingBy(
						x -> (int) (((Math.abs(x.getEndTime().getTime() - x.getStartTime().getTime())) / (1000 * 60))
								% 60),
						Collectors.counting())));
		List<String> act5 = new ArrayList<String>();
		activity5Min.forEach((activity, duration) -> {

			long min5 = 0;
			long total = 0;
			for (Object min : duration.keySet()) {

				if ((Long) duration.get(min) > 5)
					min5 += duration.get(min);
				total += duration.get(min);
			}
			if (min5 >= total * 90 / 100) {
				//task6.println(activity);
				act5.add(activity);
			}
		});
		task6.println(act5.toString());
		task6.close();
	}

	private static Date toDate(String d) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
		Date data = null;
		try {
			data = format.parse(d);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return data;
	}

}
